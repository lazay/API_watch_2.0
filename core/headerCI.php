<body onload="ShowPicture();">
<div class="row">
  <nav class="navbar navbar-inverse navbar-static-top" id='nav'>
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed menu-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class='nav navbar-nav col-md-12'>
          <li>
            <a href='../index.php' title='' class="text-uppercase text-center">home</a>
          </li>
          <li>
            <a href='./templates/stat.php' title='' class="text-uppercase text-center">stats</a>
          </li>
          <li class="dropdown">
            <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
            role="button" aria-haspopup="true" aria-expanded="false">watchs<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=1">Web</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=2">Sofware</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=3">Hardware</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=4">Mobile</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=5">Development</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=6">creative</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=7">Gaming</a></li>
              <li role="separator" class="divider"></li>
              <li class="text-uppercase text-center"><a href="../templates/categoryindex.php?idsubject=8">Other</a></li>
            </ul>
          </a>
        </li>
        <li>

          <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#random">random</a>
        </li>
        <li class="dropdown">
          <a href='' title='' class="dropdown-toggle text-uppercase text-center" data-toggle="dropdown"
          role="button" aria-haspopup="true" aria-expanded="false">users<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li class="text-uppercase text-center"><a href="">you must be log to access users</a></li>
          </ul>
        </a>
      </li>
      <li class="navbar-right">
        <a href='' title='' class="text-uppercase text-center" data-toggle="modal" data-target="#account">log in</a>
      </li>
    </ul>
  </div>
</div>
</nav>
</div>

<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-labelledby="Connexion">
<div class="modal-dialog modal-sm" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="Connexion">Connexion</h4>
    </div>
    <div class="modal-body">
      <form action="../includes/login.php" method="get">
        <input name='username' type="text" placeholder="Username">
        <label for='username'></label>
        <input name='password' type="password" placeholder="Password">
        <label for='password'></label>
    </div>
    <div class="modal-footer">
      <input class="btn btn-primary" type="submit" value='Log in'>
      <h4 class="text-center"> Don't have an account yet ? </h4>
      <a href='./templates/sign.php' title=''><button type="button" class="btn btn-default sign">Sign up</button></a>
    </div>
    </form>
  </div>
</div>
</div>


<div class="modal fade" id="random" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onClick="window.location.reload();" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Spin the watch wheel</h4>
      </div>
        <div id="pic"></div>
        <input class="btn btn-primary" type="button" value="RANDOM" onclick="GetValue();" />
        <div id="message"></div>
        <audio id="audio" src="SF-snare.mp3"></audio>
      </div>
    </div>
  </div>
</div>
<script src="../core/random.js"></script>
</body>
