<?php
include("../config/config.php");

function detail() {
global $handle;

$idwatch = $_GET["id"];

$sql = "SELECT * FROM watch WHERE id=$idwatch";
$watch = mysqli_fetch_array(mysqli_query($handle,$sql));

$query="SELECT * FROM content WHERE idwatch=$idwatch";
$content=mysqli_fetch_array(mysqli_query($handle,$query));

$query="SELECT u.name , u.firstname FROM users u INNER JOIN watch w ON u.id = w.iduser WHERE w.id=$idwatch";
$name=mysqli_fetch_array(mysqli_query($handle,$query));

echo "<div class='row'><h2 class='text-uppercase text-center'>".$watch["title"]."</h2></div>";
echo "<div class='row'><div class='col-md-4 col-md-offset-2'><h4 class='text-uppercase'><b>Posted by :&nbsp</b>"
.$name["firstname"]."&nbsp".$name["name"]."</h4></div>";
echo "<div class='col-md-4'><h4 class='text-uppercase pull-right'><b>Date :&nbsp</b> "
.$watch["ddate"]."</h4></div></div></br>";
echo "<div class='row'><p class='detail col-md-8 col-md-offset-2 text-center'>".$content["value"]."</p></div>";
echo "<div class='row'><div class='col-md-2 col-md-offset-3'><a href='".$content["source"].
"'><button class='btn btn-primary btn-block'><span class='glyphicon glyphicon-link' aria-hidden='true'>
</span> SOURCE</button></a></div>";

if ($content["name"]== NULL){
  echo "<div class='col-md-2 col-md-offset-2'><a href=''>
  <button class='btn btn-primary btn-block disabled'><span class='glyphicon glyphicon-download-alt' aria-hidden='true'>
  </span> DOWNLOAD</button></a></div></div>";
}
else {
  echo "<div class='col-md-2 col-md-offset-2'><a href='../uploads/' download='".$content["name"]."'>
  <button class='btn btn-primary btn-block'><span class='glyphicon glyphicon-download-alt' aria-hidden='true'>
  </span> DOWNLOAD</button></a></div></div>";
}


}

 ?>
