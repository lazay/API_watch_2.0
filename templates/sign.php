<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
  <?php
  include("./config/config.php");
  ?>

<div class='container-fluid'>
  <?php
  include("../core/headerCI.php");
  ?>

<form class="col-md-6 col-md-offset-3" action="../includes/account.php" method="get">
  <input name='name' type="text" placeholder="Name">
  <label for="name"></label>
  <input name='firstname' type="text" placeholder="First Name">
  <label for="firstname"></label>
  <!--<select name='promo' class="form-control promo-log">
    <option selected="selected">ada-lovelace</option>
    <option >alan-turing</option>
  </select>-->
  <input name='mail' type="text" placeholder="Mail">
  <label for="mail"></label>
  <input name='username' type="text" placeholder="Username">
  <label for="username"></label>
  <input name='password' type="password" placeholder="Password">
  <label for="password"></label>
  <input name='cfpassword' type="password" placeholder="Confirm Password">
  <label for="cfpassword"></label>
  <input class="btn btn-primary" type="submit" value='Sign in'>
</form>

</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
