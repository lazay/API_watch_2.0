<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
<div class='container-fluid'>
<?php
include("../config/config.php");
session_start();
include("../core/headerW.php");
include("../includes/userinfo.php");
?>
<div class='col-md-3 text-center'>
  <a href='delete.php' class="btn btn-primary">Delete account</a></br>
</div>
</div>


<?php
include("../core/showuser.php");
?>
</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
