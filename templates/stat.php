<?php
include ('stat_category.php');
?>

<!DOCTYPE HTML>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="template/style.css">

  <script type="text/javascript" src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body>
  <div id="chartContainer" style="height: 300px; width: 100%;"></div>
  <div class='row text-center'>
    <a href='../index.php' class="btn btn-default">Back</a></br>
  </div>
  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {
      title:{
        text: "Watches posted by category"
      },
      data: [

        {
          dataPoints: [
            { y: <?php echo $resultat_web[0] ; ?>, label: "Web"},
            { y: <?php echo $resultat_software[0] ; ?>,  label: "Software" },
            { y: <?php echo $resultat_hardware[0] ; ?>,  label: "Hardware"},
            { y: <?php echo $resultat_mobile[0] ; ?>,  label: "Mobile"},
            { y: <?php echo $resultat_dev[0] ; ?>,  label: "Developpement"},
            { y: <?php echo $resultat_creative[0] ; ?>, label: "Creative"},
            { y: <?php echo $resultat_gaming[0] ; ?>,  label: "Gaming"},
            { y: <?php echo $resultat_others[0] ; ?>,  label: "Others"}
          ]
        }
      ]
    });

    chart.render();
  }
  </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
