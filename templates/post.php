<!doctype html>
<html>
<head>
  <meta charset="utf-8">
   <title>POPWatch</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
   integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
   <link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
  <?php
  include("../config/config.php");
  session_start();
  include("../core/headerW.php");
  ?>

<form  class="col-md-6 col-md-offset-3" action="../core/watchpost.php" method="post" enctype="multipart/form-data">
  <input name='title' type="text" placeholder="Title">
  <input id='three' name='keywords' type="text" placeholder="Keywords">


  <textarea id='four' name='value' class='form-control' rows='6' placeholder="Description"></textarea>

  <select id='five' name='subject' class="form-control">
    <option value=1 selected="selected">web</option>
    <option value=2>software</option>
    <option value=3>hardware</option>
    <option value=4>mobile</option>
    <option value=5>development</option>
    <option value=6>creative</option>
    <option value=7>gaming</option>
    <option value=8>others</option>
  </select>

  <input id='six' name='source' type="text" placeholder="Source link">
	<br>
	<br>
 	<input type="file" name="userfile">
  
  <input id='two' class="btn btn-primary" type="submit" value='Post'>
</form>

</div>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
